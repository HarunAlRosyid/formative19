package com.nexsoft.elibrary.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;
import com.nexsoft.elibrary.entity.Books;

@Repository
@Transactional
public class BooksRepository {
	@PersistenceContext
	EntityManager entityManager;
	
	public List<Books> getAll() { 
		TypedQuery<Books> query =
			      entityManager.createQuery("SELECT b FROM Books b", Books.class);
			  List<Books> results = query.getResultList();
			  return results;
    }
	public List<Books> getById(int id) { 
		TypedQuery<Books> query =
			      entityManager.createQuery("SELECT b FROM Books b WHERE id="+id, Books.class);
			  List<Books> results = query.getResultList();
			  return results;
    }

}
