package com.nexsoft.elibrary.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nexsoft.elibrary.entity.AuthorBooks;
import com.nexsoft.elibrary.jpa.AuthorBooksRepository;
import com.nexsoft.elibrary.view.AuthorBooksView;

@Component
public class AuthorBooksController {
	@Autowired
	AuthorBooksRepository repo;
	@Autowired
	AuthorBooksView view;
	
	public void allAuthorBooks() {
		List<AuthorBooks> result = repo.getAll();
		view.showAll(result);
	}


	public void bookByAuthor(String author) {
		List<AuthorBooks> result = repo.getByAuthors(author);
		view.showAll(result);
	}
}
