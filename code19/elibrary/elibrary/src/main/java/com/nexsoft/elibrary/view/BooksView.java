package com.nexsoft.elibrary.view;

import java.util.List;

import org.springframework.stereotype.Component;

import com.nexsoft.elibrary.entity.Books;
@Component
public class BooksView {

	public void showAll(List<Books> result){
		breakln();
		System.out.println("List Of Books");
		for (int i = 0 ; i<result.size(); i++) {
			System.out.println( 
					"ID :" + result.get(i).getId()
					+ ", Title: " + result.get(i).getTitle()
					+ ", Publisher: " + result.get(i).getPublisher());
		}
	}
	public void breakln(){
		System.out.println("-----------------------------------------------");
	}

	public void showById(List<Books> result) {
		breakln();
		System.out.println("Detail Of Books");
		System.out.println( 
				"ID :" + result.get(0).getId()
				+ ", Title: " + result.get(0).getTitle()
				+ ", Publisher: " + result.get(0).getPublisher());
		
	}
}
