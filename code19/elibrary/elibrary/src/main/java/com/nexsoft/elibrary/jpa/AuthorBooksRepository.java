package com.nexsoft.elibrary.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.nexsoft.elibrary.entity.AuthorBooks;
@Repository
@Transactional
public class AuthorBooksRepository {
	@PersistenceContext
	EntityManager entityManager;
	
	public List<AuthorBooks> getAll() { 
		TypedQuery<AuthorBooks> query =
			      entityManager.createQuery("SELECT ab FROM AuthorBooks ab", AuthorBooks.class);
			  List<AuthorBooks> results = query.getResultList();
			  return results;
    }

	public List<AuthorBooks> getByAuthors(String author) { 
		TypedQuery<AuthorBooks> query =
			      entityManager.createQuery("SELECT ab FROM AuthorBooks ab WHERE author='"+author+"'", AuthorBooks.class);
			  List<AuthorBooks> results = query.getResultList();
			  return results;
    }
}
