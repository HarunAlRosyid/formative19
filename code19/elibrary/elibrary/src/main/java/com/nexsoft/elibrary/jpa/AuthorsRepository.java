package com.nexsoft.elibrary.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.nexsoft.elibrary.entity.Authors;


@Repository
@Transactional
public class AuthorsRepository {
	@PersistenceContext
	EntityManager entityManager;
	
	public List<Authors> getAll() { 
		TypedQuery<Authors> query =
			      entityManager.createQuery("SELECT a FROM Authors a", Authors.class);
			  List<Authors> results = query.getResultList();
			  return results;
    }
	public List<Authors> getById(int id) { 
		TypedQuery<Authors> query =
			      entityManager.createQuery("SELECT a FROM Authors a WHERE id="+id, Authors.class);
			  List<Authors> results = query.getResultList();
			  return results;
    }

}
