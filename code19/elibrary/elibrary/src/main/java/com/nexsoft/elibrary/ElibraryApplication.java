package com.nexsoft.elibrary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.nexsoft.elibrary.controller.BooksController;
import com.nexsoft.elibrary.controller.MainController;
import com.nexsoft.elibrary.jpa.BooksRepository;

@SpringBootApplication
public class ElibraryApplication implements CommandLineRunner {
	@Autowired
	MainController main;

	public static void main(String[] args) {
		SpringApplication.run(ElibraryApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		main.run();
	}

}
