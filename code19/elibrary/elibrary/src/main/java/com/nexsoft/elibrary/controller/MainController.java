package com.nexsoft.elibrary.controller;

import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nexsoft.elibrary.entity.Books;
import com.nexsoft.elibrary.view.MainView;
@Component
public class MainController {
	@Autowired
	BooksController books;
	@Autowired
	AuthorsController authors;
	@Autowired
	AuthorBooksController authorbooks;
	@Autowired
	MainView view;
	
	
	Scanner input = new Scanner(System.in);

	public void run() {

		view.listMenu();
		try {
			int menu = input.nextInt();
			while (menu != 7) {
	            switch (menu) {
	                case 1:
	                	books.allBooks();
	                    break;
	                case 2:
	                	findByIdBook();
	                    break;
	                case 3:
	                	authors.allAuthors();
	                    break;
	                case 4:
	                	findByIdAuthor();
	                    break;
	                case 5:
	                	authorbooks.allAuthorBooks();
	                    break;
	                case 6:
	                	findByAuthor();
	                    break;
	                case 7:
	                	System.out.println("Shutdown");
	        			System.exit(0);
	                    break;
	                default:
	                   System.out.println("Please Choose 1-7");
	            }
	            view.listMenu();	
	            menu = input.nextInt();
	        }
			
		}catch(Exception e) {
			System.out.println("Please Choose 1-7");
			System.out.println("Shutdown");
			System.exit(0);
		}
	}
	
	public void findByIdBook() {
		System.out.println("Input Id Book");
		int id = input.nextInt();
		String idBook=String.valueOf(id);  
		if(Pattern.compile("[0-9]").matcher(idBook).matches()==true) {
			books.bookById(id);
		} else {
			System.out.println("Your Input Invalid");
		}		
	}
	
	public void findByIdAuthor() {
		System.out.println("Input Id author");
		int id = input.nextInt();
		String idAuthor=String.valueOf(id);  
		if(Pattern.compile("[0-9]").matcher(idAuthor).matches()==true) {
			authors.authorsById(id);
		} else {
			System.out.println("Your Input Invalid");
		}
		
	}
	public void findByAuthor() {
		System.out.println("Input Name author");
		String author = input.next();
//		if(Pattern.compile("[a-zA-Z]{0,50}").matcher(author).matches()==true) {
			authorbooks.bookByAuthor(author);
//		} else {
//			System.out.println("Your Input Invalid");
//		}
	}
	
	
	
	
}
