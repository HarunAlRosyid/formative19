package com.nexsoft.elibrary.view;

import org.springframework.stereotype.Component;

@Component
public class MainView {
	
	public void listMenu() {
		String[] menu = {
				"Show All Books", "Find Books By Id", 
				"Show All Author","Find Authors By Id", 
				"Show All Authors Books","Find Books By Id",
				"Exit"};
        
        System.out.println("--------------------Elibrary-------------------");
        
        for (int i = 0; i < menu.length; i++) {
            System.out.println(i + 1 + "." + menu[i]);
        }
        System.out.println("Input Menu");
	}
}