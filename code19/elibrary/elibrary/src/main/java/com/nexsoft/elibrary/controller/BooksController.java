package com.nexsoft.elibrary.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nexsoft.elibrary.entity.Books;
import com.nexsoft.elibrary.jpa.BooksRepository;
import com.nexsoft.elibrary.view.BooksView;
@Component
public class BooksController {
	@Autowired
	BooksRepository repo;
	@Autowired
	BooksView view;

	public void allBooks() {
		List<Books> result = repo.getAll();
		view.showAll(result);
	}
	public void bookById(int id) {
		List<Books> result = repo.getById(id);
		view.showById(result);
	}
}
