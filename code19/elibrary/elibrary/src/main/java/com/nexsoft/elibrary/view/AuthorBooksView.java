package com.nexsoft.elibrary.view;

import java.util.List;

import org.springframework.stereotype.Component;

import com.nexsoft.elibrary.entity.AuthorBooks;

@Component
public class AuthorBooksView {

	public void showAll(List<AuthorBooks> result){
		breakln();
		System.out.println("List Of Author Books");
		for (int i = 0 ; i<result.size(); i++) {
			System.out.println( 
					"ID :" + result.get(i).getId()
					+ ", Book: " + result.get(i).getBook()
					+ ", Author: " + result.get(i).getAuthor());
		}
	}
	public void breakln(){
		System.out.println("-----------------------------------------------");
	}
	public void showByAuthor(List<AuthorBooks> result) {
		breakln();
		System.out.println(result.toString());
		for (int i = 0 ; i<result.size(); i++) {
			System.out.println( 
					"ID :" + result.get(i).getId()
					+ ", Book: " + result.get(i).getBook()
					+ ", Author: " + result.get(i).getAuthor());
		}
		
	}
}
