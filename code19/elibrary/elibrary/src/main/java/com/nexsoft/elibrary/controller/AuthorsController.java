package com.nexsoft.elibrary.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nexsoft.elibrary.entity.Authors;
import com.nexsoft.elibrary.jpa.AuthorsRepository;
import com.nexsoft.elibrary.view.AuthorsView;

@Component
public class AuthorsController {
	@Autowired
	AuthorsRepository repo;
	@Autowired
	AuthorsView view;
	
	public void allAuthors() {
		List<Authors> result = repo.getAll();
		view.showAll(result);
	}
	public void authorsById(int id) {
		List<Authors> result = repo.getById(id);
		view.showById(result);
	}
}
