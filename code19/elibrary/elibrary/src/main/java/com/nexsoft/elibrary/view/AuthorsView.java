package com.nexsoft.elibrary.view;

import java.util.List;

import org.springframework.stereotype.Component;

import com.nexsoft.elibrary.entity.Authors;

@Component
public class AuthorsView {
	
	public void showAll(List<Authors> result){
		breakln();
		System.out.println("List Of Authors");
		for (int i = 0 ; i<result.size(); i++) {
			System.out.println( 
					"ID :" + result.get(i).getId()
					+ ", Name: " + result.get(i).getName());
		}
	}
	public void breakln(){
		System.out.println("-----------------------------------------------");
	}
	public void showById(List<Authors> result) {
		breakln();
		System.out.println("Detail Of Authors");
		System.out.println( 
				"ID :" + result.get(0).getId()
				+ ", Title: " + result.get(0).getName());
		
	}
}
