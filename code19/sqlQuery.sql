CREATE DATABASE elibrary;

USE elibrary;
CREATE TABLE books(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
    title VARCHAR(50) NOT NULL ,
    publisher VARCHAR(50) NOT NULL
);
CREATE TABLE authors(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
    name VARCHAR(50) NOT NULL 
);

CREATE TABLE author_books(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
    book_id INT NOT NULL ,
    author_id INT NOT NULL ,
    KEY book_id (`book_id`),
	CONSTRAINT book_id FOREIGN KEY (`book_id`) REFERENCES books (`id`),
    KEY author_id (`author_id`),
	CONSTRAINT author_id FOREIGN KEY (`author_id`) REFERENCES authors (`id`)
);

INSERT INTO books(title,publisher) 
VALUES
("Book1", "Publisher1"),
("Book2", "Publisher2"),
("Book3", "Publisher3"),
("Book4", "Publisher4");

INSERT INTO authors(name) 
VALUES
("Author1"),
("Author2");

INSERT INTO author_books(book_id,author_id) 
VALUES
("1", "1"),
("2", "1"),
("3", "2"),
("4", "2");

-- getall books
SELECT id, title, publisher 
	FROM books;
-- findbyid books
SELECT id, title, publisher 
	FROM books
    WHERE id=2;

-- getall authors
SELECT id, name FROM authors;
-- findbyid authors
SELECT id, name 
	FROM authors
    WHERE id=2;
-- getbooksfromauthorid
SELECT author_books.id, books.title, authors.name 
	FROM author_books 
		JOIN books ON books.id =author_books.book_id
		JOIN authors ON authors.id =author_books.author_id;
        
CREATE VIEW author_books_view AS SELECT author_books.id , books.title as book, authors.name as author
	FROM author_books 
		JOIN books ON books.id =author_books.book_id
		JOIN authors ON authors.id =author_books.author_id;
